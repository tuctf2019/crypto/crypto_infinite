# Crypto Infinite -- TUCTF2019

The challenge prompts you for input and then gives you the ciphertext. The intended solution is to craft input in such a way that reveals what the cipher is doing.

Solve script: `solve.py`

Flag: `TUCTF{1nf1n1t3_1s_n0t_4_g00d_n4m3}`

## The Ciphers

0. Just pigpen
1. Physically rotates the pigpen to the right. i.e. ^ -> >, > -> v, and so on.
2. Rotate the symbols of the pigpen to the right in the following order with wrapping: `['_', '|', ']', '[', '-', '.', 'v', '>', '<', '^']`
3. Caesar cipher with key 69 (nice)
4. Keyed Caesar cipher with key `TUCTFINFINITE`
5. Vigenere with key `INFINITE`
6. Matrix cipher with key of 4
7. Caesar Cipher with key 420 (also nice) then physically rotate right (like level 1)
8. Vigenere Cipher with key `ETERNAL` then rotate symbols right (like level 2)
9. The monster level: Keyed Caesar with key `TRUECRYPTOPRO` -> Matrix -> Rotate Right -> Rotate Symbols Right
