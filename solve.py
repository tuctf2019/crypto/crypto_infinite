#!/usr/bin/env python2

import sys, string

from pwn import *

from pigpen import PigPen

def level_zero_decrypt(enc):
    return PigPen.from_pigpen_string(enc).decipher()

def level_one_decrypt(enc):
    return PigPen.from_pigpen_string(enc).rotate_left().decipher()

def level_two_decrypt(enc):
    return PigPen.from_pigpen_string(enc).rotate_symbols_left().decipher()

def level_three_decrypt(enc):
    revkey = 26 - (69 % 26)
    cipher = PigPen.from_pigpen_string(enc).decipher()
    return ''.join([chr(((ord(c) - ord('A')) + revkey) % 26 + ord('A')) for c in cipher])

def level_four_decrypt(enc):
    k = 'TUCTFINFINITE'

    cipher = PigPen.from_pigpen_string(enc).decipher()
    
    # normal alphabet
    na = string.ascii_uppercase
    # keyed alphabet
    ka = ''.join(OrderedDict.fromkeys(k + na))
    assert len(ka) == len(na)

    keymap = dict(zip(list(ka), list(na)))
    keymap[' '] = ' '

    return ''.join([keymap[c] for c in cipher])

def level_five_decrypt(enc):
    k = 'INFINITE'

    cipher = PigPen.from_pigpen_string(enc).decipher()
    ki = [(26 - (ord(c) - ord('A'))) for c in k]

    o = ''
    for i in range(len(cipher)):
        o += chr(((ord(cipher[i]) - ord('A')) + ki[i % len(ki)]) % 26 + ord('A'))

    return o

def level_six_decrypt(enc):
    cipher = PigPen.from_pigpen_string(enc).decipher()
    l = len(cipher)
    ll = l // 4
    lm = l % 4
    if lm == 0:
        o = ''
        for i in xrange(ll):
            o += cipher[i::ll]
        return o

    ll += 1
    need = ll * 4
    fill = 4 - (need - l)
    mat = ['']*ll
    ci = 0
    col = 0
    row = 0
    for i in xrange(need):
        col = i % ll
        if col == 0 and i != 0:
            row += 1
        if row < fill:
            mat[col] += cipher[ci]
            ci += 1
        else:
            if col == ll - 1:
                continue
            else:
                mat[col] += cipher[ci]
                ci += 1
        if ci == l:
            break
    o = ''.join(mat)
    return o

def level_seven_decrypt(enc):
    cipher = PigPen.from_pigpen_string(enc).rotate_left().decipher()
    revkey = 26 - (420 % 26)
    return ''.join([chr(((ord(c) - ord('A')) + revkey) % 26 + ord('A')) for c in cipher])

def level_eight_decrypt(enc):
    cipher = PigPen.from_pigpen_string(enc).rotate_symbols_left().decipher()

    k = "ETERNAL"
    ki = [(26 - (ord(c) - ord('A'))) for c in k]

    o = ''
    for i in range(len(cipher)):
        o += chr(((ord(cipher[i]) - ord('A')) + ki[i % len(ki)]) % 26 + ord('A'))

    return o

def level_nine_decrypt(enc):
    k = 'TRUECRYPTOPRO'

    cipher = PigPen.from_pigpen_string(enc).rotate_symbols_left().rotate_left().decipher()

    l = len(cipher)
    ll = l // 4
    lm = l % 4
    if lm == 0:
        o = ''
        for i in xrange(ll):
            o += cipher[i::ll]
    else:
        ll += 1
        need = ll * 4
        fill = 4 - (need - l)
        mat = ['']*ll
        ci = 0
        col = 0
        row = 0
        for i in xrange(need):
            col = i % ll
            if col == 0 and i != 0:
                row += 1
            if row < fill:
                mat[col] += cipher[ci]
                ci += 1
            else:
                if col == ll - 1:
                    continue
                else:
                    mat[col] += cipher[ci]
                    ci += 1
            if ci == l:
                break
        o = ''.join(mat)
    
    # normal alphabet
    na = string.ascii_uppercase
    # keyed alphabet
    ka = ''.join(OrderedDict.fromkeys(k + na))
    assert len(ka) == len(na)

    keymap = dict(zip(list(ka), list(na)))
    keymap[' '] = ' '

    return ''.join([keymap[c] for c in o])

# TODO Change address
r = remote('chal.tuctf.com', '30102')


# Level 0

r.recvuntil('Give me text:\n')
r.send('text\n')

for i in range(50):
    r.recvuntil('Decrypt ')
    m = r.recvuntil('\n')[:-1]
    d = level_zero_decrypt(m)
    r.send('{}\n'.format(d))


# Level 1

r.recvuntil('Give me text:\n')
r.send('text\n')

for i in range(50):
    r.recvuntil('Decrypt ')
    m = r.recvuntil('\n')[:-1]
    d = level_one_decrypt(m)
    r.send('{}\n'.format(d))


# Level 2

r.recvuntil('Give me text:\n')
r.send('text\n')

for i in range(50):
    r.recvuntil('Decrypt ')
    m = r.recvuntil('\n')[:-1]
    d = level_two_decrypt(m)
    r.send('{}\n'.format(d))


# Level 3

r.recvuntil('Give me text:\n')
r.send('text\n')

for i in range(50):
    r.recvuntil('Decrypt ')
    m = r.recvuntil('\n')[:-1]
    d = level_three_decrypt(m)
    r.send('{}\n'.format(d))


# Level 4

r.recvuntil('Give me text:\n')
r.send('text\n')

for i in range(50):
    r.recvuntil('Decrypt ')
    m = r.recvuntil('\n')[:-1]
    d = level_four_decrypt(m)
    r.send('{}\n'.format(d))


# Level 5

r.recvuntil('Give me text:\n')
r.send('text\n')

for i in range(50):
    r.recvuntil('Decrypt ')
    m = r.recvuntil('\n')[:-1]
    d = level_five_decrypt(m)
    r.send('{}\n'.format(d))


# Level 6

r.recvuntil('Give me text:\n')
r.send('text\n')

for i in range(50):
    r.recvuntil('Decrypt ')
    m = r.recvuntil('\n')[:-1]
    d = level_six_decrypt(m)
    r.send('{}\n'.format(d))


# Level 7

r.recvuntil('Give me text:\n')
r.send('text\n')

for i in range(50):
    r.recvuntil('Decrypt ')
    m = r.recvuntil('\n')[:-1]
    d = level_seven_decrypt(m)
    r.send('{}\n'.format(d))


# Level 8

r.recvuntil('Give me text:\n')
r.send('text\n')

for i in range(50):
    r.recvuntil('Decrypt ')
    m = r.recvuntil('\n')[:-1]
    d = level_eight_decrypt(m)
    r.send('{}\n'.format(d))


# Level 9

r.recvuntil('Give me text:\n')
r.send('text\n')

for i in range(50):
    r.recvuntil('Decrypt ')
    m = r.recvuntil('\n')[:-1]
    d = level_nine_decrypt(m)
    r.send('{}\n'.format(d))


r.recvuntil('Here\'s your flag:\n')

flag = r.recvuntil('}\n')[:-1]

FLAG = 'TUCTF{1nf1n1t3_1s_n0t_4_g00d_n4m3}'

r.close()

if flag == FLAG:
    print 'gucchi'
    sys.exit(0)
print 'no flag'
sys.exit(1)
