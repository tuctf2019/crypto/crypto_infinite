# Crypto Infinite

Desc: `Crypto has been around forever. Does this challenge go on forever? Let us know in the comments below or find the flag to prove us wrong.`

Flag: `TUCTF{1nf1n1t3_1s_n0t_4_g00d_n4m3}`
